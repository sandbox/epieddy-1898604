<?php

/**
 * @file
 * TODO.
 */

/**
 * Form builder.
 */
function workflow_form($form, &$form_state, $node) {

  global $user;

  $form = array();
  $form['#node'] = $node;
  $form['#user'] = $user;
  $states = workflow_get_states($node->type);
  $new_states = work_get_new_states($node, $user);

  $header = array(

    t('Step'),
    t('Date'),
    t('State'),
    t('Author'),
    t('Comment'),
  );

  // Transition
  $form['transition'] = array(

    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('New state'),
  );

  if (empty($new_states)) {
    $form['transition']['text'] = array(

      '#markup' => '<p>' . t('Your are not allowed to move to a new state.') . '</p>',
    );
  }
  else {

    $form['transition']['table'] = array(

      '#theme' => 'table',
      '#header' => $header,
      '#suce' => TRUE,
    );

    $row = array();
    $row[] = count($node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['history']) + 1;
    $row[] = format_date(time(), 'short');

    $form['transition']['table']['new_state'] = array(

      '#type' => 'select',
      '#title' => '',
      '#options' => $new_states,
      '#required' => TRUE,
    );

    if (isset($form_state['storage']['new_state'])) {

      $form['transition']['table']['new_state']['#value'] = $form_state['storage']['new_state'];
      $form['transition']['table']['new_state']['#disabled'] = TRUE;
    }

    $row[] = array('data' => &$form['transition']['table']['new_state']);
    $row[] = format_username($user);

    $form['transition']['table']['comment'] = array(

      '#type' => 'textarea',
      '#title' => '',
    );

    if (isset($form_state['storage']['comment'])) {

      $form['transition']['table']['comment']['#value'] = $form_state['storage']['comment'];
      $form['transition']['table']['comment']['#disabled'] = TRUE;
    }

    $row[] = array('data' => &$form['transition']['table']['comment']);

    $form['transition']['table']['#rows'] = array($row);

    if (isset($form_state['storage']['confirm'])) {

      $form['transition']['#collapsed'] = FALSE;
      return confirm_form($form, t('Confirm the creation of a new state ?'), 'node/' . $form['#node']->nid . '/workflow');
    }

    $form['transition']['submit'] = array(

      '#type' => 'submit',
      '#value' => t('Move to new state'),
      '#submit' => array('workflow_transition_confirm'),
    );
  }

  // History
  $form['history'] = array(

    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('History'),
  );

  $rows = array();
  foreach ($node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['history'] as $step => $change) {

    $row = array();

    $row[] = $step + 1;

    $row[] = format_date($change['date'], 'short');
    $row[] = $states[$change['state']];

    $account = user_load($change['uid']);
    $row[] = format_username($account);

    $row[] = nl2br(check_plain($change['comment']));

    $rows[] = $row;
  }

  $form['history']['table'] = array(

    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $form;
}

/**
 * Submit handler.
 */
function workflow_transition_confirm($form, &$form_state) {

  $form_state['storage'] = $form_state['values'] + array('comment' => '');
  $form_state['storage']['confirm'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler.
 */
function workflow_form_submit($form, &$form_state) {

  $node = $form['#node'];

  $previous_state = $node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['state'];
  $node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['state'] = $form_state['storage']['new_state'];
  $node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['history'][] = array(

    'date' => time(),
    'state' => $form_state['storage']['new_state'],
    'uid' => $form['#user']->uid,
    'comment' => $form_state['storage']['comment'],
  );

  field_attach_presave('node', $node);
  field_attach_update('node', $node);

  node_access_acquire_grants($node);

  drupal_set_message(t('New state saved.'));

  module_invoke_all('workflow_transition', $node, $previous_state, $form_state['storage']['new_state'], $form['#user']->uid, $form_state['storage']['comment']);
}
