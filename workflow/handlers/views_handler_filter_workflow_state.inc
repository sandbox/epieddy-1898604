<?php

/**
 * @file
 * Definition of views_handler_filter_workflow_state.
 */

/**
 * Filter by node type.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_workflow_state extends views_handler_filter_in_operator {

  function get_value_options() {

    if (!isset($this->value_options)) {

      $this->value_title = t('Workflow State');
      $types = node_type_get_types();

      $options = array();
      foreach ($types as $type => $info) {
        $options += workflow_get_states($type);
      }

      $this->value_options = $options;
    }
  }
}
