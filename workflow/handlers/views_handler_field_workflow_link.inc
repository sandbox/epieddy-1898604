<?php

/**
 * @file
 * Definition of views_handler_field_workflow_link.
 */

/**
 * Field handler to present a link node workflow.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_workflow_link extends views_handler_field_node_link {

  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to edit this node.
    if (!workflow_form_access($node)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = 'node/' . $node->nid . '/workflow';
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return $text;
  }
}
