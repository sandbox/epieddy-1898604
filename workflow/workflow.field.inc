<?php

/**
 * @file
 * Workflow
 */

// Fields type defines.
define('WORKFLOW_FIELD_TYPE', 'workflow');

// Fields name defines.
define('WORKFLOW_FIELD_NAME', 'field_workflow');

/* Fields */

/**
 * Implements hook_field_info().
 */
function workflow_field_info() {

  return array(

    WORKFLOW_FIELD_TYPE => array(

      'label' => t('Workflow'),
      'description' => t('This field allow to add Workflow to an entity.'),
      'default_widget' => 'workflow_widget',
      'default_formatter' => 'workflow_formatter',
      'no_ui' => TRUE,
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function workflow_field_is_empty($item, $field) {

  switch ($field['type']) {

    case WORKFLOW_FIELD_TYPE:
      if (isset($item['state']) && !empty($item['state'])) {
        return FALSE;
      }
      break;
 }

  return TRUE;
}

/**
 * Implements hook_field_presave().
 */
function workflow_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {

  switch ($field['type']) {

    case WORKFLOW_FIELD_TYPE:
      foreach ($items as &$item) {

        if (!is_string($item['history'])) {
          $item['history'] = serialize($item['history']);
        }
      }
      break;
  }
}

/**
 * Implements hook_field_load().
 */
function workflow_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {

  switch ($field['type']) {

    case WORKFLOW_FIELD_TYPE:
      foreach ($entities as $id => $entity) {
        foreach ($items[$id] as $delta => $item) {

          if (is_string($item['history'])) {
            $items[$id][$delta]['history'] = unserialize($item['history']);
          }

          if (!is_array($items[$id][$delta]['history'])) {
            $items[$id][$delta]['history'] = array();
          }
        }
      }
      break;
  }
}

/* Widgets */

/**
 * Implements hook_field_widget_info().
 */
function workflow_field_widget_info() {

  return array(

    'workflow_widget' => array(

      'label' => t('Workflow Widget'),
      'field types' => array(WORKFLOW_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function workflow_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {

    case 'workflow_widget':
      // DO NOTHING
      break;
  }

  return $element;
}

/* Formatters */

/**
 * Implements hook_field_formatter_info().
 */
function workflow_field_formatter_info() {

  return array(

    'workflow_formatter' => array(

      'label' => t('Default'),
      'field types' => array(WORKFLOW_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function workflow_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  if ($entity_type != 'node') {
    return array();
  }

  $element = array();

  switch ($display['type']) {

    case 'workflow_formatter':

      $states = workflow_get_states($entity->type);
      foreach ($items as $delta => $item) {
        $element[$delta]['#markup'] = isset($states[$item['state']]) ? $states[$item['state']] : '';
      }
      break;
  }

  return $element;
}
