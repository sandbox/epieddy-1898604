<?php

/**
 * @file
 * Definition of views_handler_filter_menu_links_parent.
 */

/**
 * TODO.
 */
class views_handler_filter_menu_links_parent extends views_handler_filter_equality {

  function can_expose() {

    return FALSE;
  }

  function option_definition() {

    $options = parent::option_definition();
    $options['depth'] = array('default' => 1);
    $options['value'] = array('default' => '0:0');

    return $options;
  }

  function operator_options() {

    return array(
      '=' => t('Is equal to'),
    );
  }

  function value_form(&$form, &$form_state) {

    $form['depth'] = array(

      '#type' => 'select',
      '#title' => t('Parent Level'),
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9)),
      '#default_value' => $this->options['depth'],
    );

    $options = array();
    $menus = menu_get_menus();
    foreach ($menus as $menu_name => $title) {

      $tmp = menu_link_built_options($menu_name);
      unset($tmp[0]);

      $options[$menu_name . ':0'] = '<' . $title . '>';
      foreach ($tmp as $mlid => $link) {
        $options[$menu_name . ':' . $mlid] = $link;
      }
    }

    $form['value'] = array(

      '#type' => 'select',
      '#title' => t('Parent Item'),
      '#default_value' => $this->value,
      '#options' => $options,
    );
  }

  function query() {

    $this->ensure_my_table();

    $field = $this->table_alias . '.p' . $this->options['depth'];
    list (,$value) = explode(':', $this->value);

    $this->query->add_where($this->options['group'], $field, $value);
  }
}
