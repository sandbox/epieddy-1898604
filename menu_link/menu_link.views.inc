<?php

/**
 * @file
 * GCC Views integration.
 */

/**
 * Implements hook_views_data().
 */
function menu_link_views_data() {

  $data = array();

  $data['menu_links']['table']['group'] = t('Menu Link');

  $data['menu_links']['parents'] = array(

    'title' => t('Parents'),
    'help' => t('Filter by link parents'),
    'filter' => array(
      'handler' => 'views_handler_filter_menu_links_parent',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function menu_link_views_data_alter(&$data) {

  $field_menu_link = field_info_field(MENU_LINK_FIELD_NAME);
  $menu_link_table = _field_sql_storage_tablename($field_menu_link);
  $menu_link_mlid_column = _field_sql_storage_columnname(MENU_LINK_FIELD_NAME, 'mlid');

  $data[$menu_link_table]['menu_link'] = array(

  'group' => t('Menu Link'),
  'relationship' => array(

    'title' => t('Menu Link Item'),
    'label' => t('Menu Link Item'),
    'help' => t('Relate the menu link item associated with the entity'),
    'base' => 'menu_links',
    'base field' => 'mlid',
    'relationship field' => $menu_link_mlid_column,
  ),
);
}
