<?php

/**
 * @file
 * Defines and handles the fields required by the CPAM Module modules.
 */

// Fields type defines.
define('MODULES_FIELD_TYPE', 'modules');

// Fields name defines.
define('MODULES_FIELD_NAME', 'field_modules');

// Other
define('MODULES_EMPTY_KEY', '_none');

/* Fields */

/**
 * Implements hook_field_info().
 */
function modules_field_info() {

  return array(

    MODULES_FIELD_TYPE => array(

      'label' => t('Module'),
      'description' => t('This field allow to add Module to an entity.'),
      'default_widget' => 'modules_widget',
      'default_formatter' => 'modules_formatter',
      'no_ui' => TRUE,
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function modules_field_is_empty($item, $field) {

  switch ($field['type']) {

    case MODULES_FIELD_TYPE:
      if (isset($item['module']) && $item['module'] != MODULES_EMPTY_KEY) {
        return FALSE;
      }
      break;
  }

  return TRUE;
}

/**
 * Implements hook_field_presave().
 */
function modules_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {

  switch ($field['type']) {

    case MODULES_FIELD_TYPE:
      foreach ($items as &$item) {

        if (!is_string($item['config'])) {
          $item['config'] = serialize($item['config']);
        }
      }
      break;
  }
}

/**
 * Implements hook_field_validate().
 */
function modules_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {

  // Do not validate on field configuration form (default value widget).
  if (!isset($entity_type) && !isset($entity)) {
    return;
  }

  switch ($field['type']) {

    case MODULES_FIELD_TYPE:

      if ($instance['required']) {

        $items = _field_filter_items($field, $items);
        if (count($items) == 0) {
          $errors[$field['field_name']][$langcode][0][] = array(

            'error' => 'required',
            'message' => t('%name: vous devez selectionner au moins 1 modules.', array('%name' => $instance['label'])),
          );
        }
      }
      break;
  }
}

/**
 * Implements hook_field_load().
 */
function modules_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {

  switch ($field['type']) {

    case MODULES_FIELD_TYPE:
      foreach ($entities as $id => $entity) {
        foreach ($items[$id] as $delta => $item) {
          if (is_string($item['config'])) {
            $items[$id][$delta]['config'] = unserialize($item['config']);
          }
        }
      }
      break;
  }
}

/* Widgets */

/**
 * Implements hook_field_widget_info().
 */
function modules_field_widget_info() {

  return array(

    'modules_widget' => array(

      'label' => t('Modules Widget'),
      'field types' => array(MODULES_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function modules_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {

    case 'modules_widget':

      $options = array(
        MODULES_EMPTY_KEY => t('Ne pas activer de modules'),
      );

      $modules = modules_get_module_list($instance['entity_type'], $instance['bundle']);
      foreach ($modules as $key => $modules) {
        $options[$key] = $modules['title'];
      }

      $element['module'] = array(

        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($items[$delta]['module']) ? $items[$delta]['module'] : MODULES_EMPTY_KEY,
        '#title' => $element['#title'],
      );

      $element['config'] = array(

        '#type' => 'value',
        '#value' => isset($items[$delta]['config']) ? $items[$delta]['config'] : array(),
      );
      break;
  }

  return $element;
}

/* Formatters */

/**
 * Implements hook_field_formatter_info().
 */
function modules_field_formatter_info() {

  return array(

    'modules_formatter' => array(

      'label' => t('Default'),
      'field types' => array(MODULES_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function modules_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  switch ($display['type']) {

    case 'modules_formatter':
      // DISPLAY NOTHING
      break;
  }

  return $element;
}
