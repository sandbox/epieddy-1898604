<?php

/**
 * @file
 * TODO.
 */

// State Key
define('WORKFLOW_CREATED', '__created');

/**
 * Retrieve the list of workflow states.
 */
function workflow_get_states($node_type) {

  $states = drupal_static(__FUNCTION__, array());

  if (!isset($states[$node_type])) {

    $states[$node_type][WORKFLOW_CREATED] = t('Created');
    $states[$node_type] += module_invoke_all('workflow_states', $node_type);
  }

  return $states[$node_type];
}

/**
 * Retrive the list a new state allowed for a given node/user.
 */
function work_get_new_states($node, $user) {

  $states = workflow_get_states($node->type);
  $current_state = $node->{WORKFLOW_FIELD_NAME}[LANGUAGE_NONE][0]['state'];

  $new_states = module_invoke_all('workflow_new_states', $node, $user, $current_state);
  $list = array();

  foreach ($new_states as $new_state) {
    if (isset($states[$new_state]) && $new_state != WORKFLOW_CREATED) {
      $list[$new_state] = $states[$new_state];
    }
  }

  return $list;
}
