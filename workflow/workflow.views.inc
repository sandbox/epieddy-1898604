<?php

/**
 * @file
 * GCC Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function workflow_views_data_alter(&$data) {

  $field_workflow = field_info_field(WORKFLOW_FIELD_NAME);
  $workflow_table = _field_sql_storage_tablename($field_workflow);
  $workflow_state_column = _field_sql_storage_columnname(WORKFLOW_FIELD_NAME, 'state');

  $data['node']['workflow_link'] = array(

    'group' => t('Workflow'),
    'field' => array(
      'title' => t('Workflow Link'),
      'help' => t('Provide a simple link to the content workflow.'),
      'handler' => 'views_handler_field_workflow_link',
    ),
  );
  
  $data[$workflow_table]['workflow_state'] = array(

    'group' => t('Workflow'),
    'real field' => $workflow_state_column,
    'filter' => array(
      'title' => t('Workflow State'),
      'help' => t('Filter by the current workflow state'),
      'handler' => 'views_handler_filter_workflow_state',
    ),
  );
}
