<?php

/**
 * @file
 * Defines and handles the fields required by the CPAM Module modules.
 */

// Fields type defines.
define('MENU_LINK_FIELD_TYPE', 'menu_link');

// Fields name defines.
define('MENU_LINK_FIELD_NAME', 'field_menu_link');

// Other
define('MENU_LINK_EMPTY_KEY', '__none');

/* Fields */

/**
 * Implements hook_field_info().
 */
function menu_link_field_info() {

  return array(

    MENU_LINK_FIELD_TYPE => array(

      'label' => t('Menu link'),
      'description' => t('This field allow to add Menu link to an entity.'),
      'default_widget' => 'menu_link_widget',
      'default_formatter' => 'menu_link_formatter',
      'no_ui' => TRUE,
      'instance_settings' => array(

        'menu_name' => array(
          'main-menu' => TRUE,
        ),
      ),
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function menu_link_field_is_empty($item, $field) {

  switch ($field['type']) {

    case MENU_LINK_FIELD_TYPE:
      if (isset($item['menu_link']['menu_name']) && $item['menu_link']['menu_name'] != MENU_LINK_EMPTY_KEY) {
        if (isset($item['menu_link']['link_title']) && trim($item['menu_link']['link_title']) != '') {
          return FALSE;
        }
      }
      break;
  }

  return TRUE;
}

/**
 * Implements hook_field_update().
 */
function menu_link_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {

  menu_link_field_save($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_insert().
 */
function menu_link_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {

  menu_link_field_save($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Helper function.
 */
function menu_link_field_save($entity_type, $entity, $field, $instance, $langcode, &$items) {

  switch ($field['type']) {

    case MENU_LINK_FIELD_TYPE:

      $current_mlids = array();
      foreach ($items as $item) {
        if ($item['mlid'] != 0) {
          $current_mlids[] = $item['mlid'];
        }
      }

      if (isset($entity->original->{MENU_LINK_FIELD_NAME}[$langcode])) {
        foreach ($entity->original->{MENU_LINK_FIELD_NAME}[$langcode] as $previous_item) {
          if ($previous_item['mlid'] != 0 && !in_array($previous_item['mlid'], $current_mlids)) {
            menu_link_delete($previous_item['mlid']);
          }
        }
      }

      foreach ($items as &$item) {

        $new_menu_link = array();

        if ($item['mlid'] != 0) {

          $menu_link = menu_link_load($item['mlid']);
          if ($menu_link) {
            $new_menu_link = $menu_link;
          }
        }

        if (isset($item['menu_link'])) {

          $new_menu_link['plid'] = $item['menu_link']['plid'];
          $new_menu_link['weight'] = $item['menu_link']['weight'];
          $new_menu_link['link_title'] = $item['menu_link']['link_title'];
          $new_menu_link['menu_name'] = $item['menu_link']['menu_name'];
          $new_menu_link['module'] = 'menu_link';

          $uri = entity_uri($entity_type, $entity);
          if ($uri) {

            $new_menu_link['link_path'] = $uri['path'];
            $item['mlid'] = menu_link_save($new_menu_link);
          }
        }
      }
      break;
  }
}

/**
 * Implements hook_field_validate().
 */
function menu_link_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {

  // Do not validate on field configuration form (default value widget).
  if (!isset($entity_type) && !isset($entity)) {
    return;
  }

  switch ($field['type']) {

    case MENU_LINK_FIELD_TYPE:
      if ($instance['required']) {

        $items = _field_filter_items($field, $items);
        if (count($items) == 0) {
          $errors[$field['field_name']][$langcode][0][] = array(

            'error' => 'required',
            'message' => t('%name: you must add at least one link.', array('%name' => $instance['label'])),
          );
        }
      }
      break;
  }
}

/**
 * Implements hook_field_delete().
 */
function menu_link_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {

  foreach ($items as $item) {
    if ($item['mlid'] != 0) {
      menu_link_delete($item['mlid']);
    }
  }
}

/**
 * Implements hook_field_instance_settings_form().
 */
function menu_link_field_instance_settings_form($field, $instance) {

  $form = array();
  $settings = $instance['settings'];

  $form['menu_name'] = array(

    '#type' => 'checkboxes',
    '#options' => menu_get_menus(),
    '#default_value' => $settings['menu_name'],
    '#title' => t('Allowed menu'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Implements hook_field_update_forbid().
 */
function menu_link_field_update_forbid($field, $prior_field, $has_data) {

  if ($field['type'] == MENU_LINK_FIELD_TYPE && $has_data) {
    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      if ($field['cardinality'] < $prior_field['cardinality'] || $prior_field['cardinality'] == FIELD_CARDINALITY_UNLIMITED) {

        $query = new EntityFieldQuery();
        $found = $query->fieldDeltaCondition($field['field_name'], $field['cardinality'] - 1, '>')->range(0, 1)->execute();
        if (!empty($found)) {
          throw new FieldUpdateForbiddenException(t('A menu link field using more than @count links exists.', array('@count' => $field['cardinality'])));
        }
      }
    }
  }
}

/* Widgets */

/**
 * Implements hook_field_widget_info().
 */
function menu_link_field_widget_info() {

  return array(

    'menu_link_widget' => array(

      'label' => t('Menu Link Widget'),
      'field types' => array(MENU_LINK_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function menu_link_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {

    case 'menu_link_widget':

      $instance['settings']['menu_name'] = array_filter($instance['settings']['menu_name']);

      // Built default value
      if (!isset($items[$delta])) {
        $items[$delta]  = array();
      }
      $items[$delta] += array(

        'menu_name' => MENU_LINK_EMPTY_KEY,
        'link_title' => '',
        'mlid' => 0,
        'plid' => 0,
        'weight' => 0,
        'options' => array(),
      );

      // Load the corresponding menu link
      $menu_link = menu_link_load($items[$delta]['mlid']);
      if ($menu_link) {
        $items[$delta] = $menu_link;
      }

      // Take the ajax values
      $ajax_items = array();
      if (isset($form_state['process_input']) && $form_state['process_input']) {

        $ajax_element = $element;
        $ajax_element['#parents'] = $element['#field_parents'];

        field_default_extract_form_values(NULL, NULL, $field, $instance, $langcode, $ajax_items, $ajax_element, $form_state);

        foreach ($ajax_items as &$ajax_item) {
          $ajax_item += $ajax_item['menu_link'];
        }
        $items = $ajax_items;
      }

      $options = menu_get_menus();
      $options = array_intersect_key($options, $instance['settings']['menu_name']);

      if (!$instance['required'] || $delta > 0) {
        $options = array(MENU_LINK_EMPTY_KEY => t('Do not create a menu link')) + $options;
      }

      // If the current menu is not allowed any more, reset some values
      if (!isset($options[$items[$delta]['menu_name']])) {

        $items[$delta]['menu_name'] = reset($instance['settings']['menu_name']);
        $items[$delta]['plid'] = 0;
        $items[$delta]['weight'] = 0;
      }

      $id = 'menu-link-menu-link-field-' . $delta;

      // Form
      $element['mlid'] = $element + array(

        '#type' => 'value',
        '#value' => $items[$delta]['mlid'],
      );

      $element['menu_link'] = array(

        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#prefix' => '<div id="' . $id . '">',
        '#suffix' => '</div>',
        '#title' => $element['#title'],
      );

      $element['menu_link']['menu_name'] = array(

        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $items[$delta]['menu_name'],
        '#title' => t('Menu'),
        '#ajax' => array(
          'callback' => 'menu_link_ajax_get_parent_list',
          'wrapper' => $id,
          'event' => 'change',
          'method' => 'replace',
        ),
        '#field-delta' => $delta,
      );

      $element['menu_link']['plid'] = array(

        '#type' => 'select',
        '#options' => menu_link_built_options($items[$delta]['menu_name'], $items[$delta]['mlid']),
        '#default_value' => $items[$delta]['plid'],
        '#title' => t('Parent'),
        '#access' => $items[$delta]['menu_name'] != MENU_LINK_EMPTY_KEY,
      );

      $element['menu_link']['link_title'] = array(

        '#type' => 'textfield',
        '#default_value' => $items[$delta]['link_title'],
        '#title' => t('Link Title'),
        '#maxlength' => 255,
        '#access' => $items[$delta]['menu_name'] != MENU_LINK_EMPTY_KEY,
      );

      $element['menu_link']['weight'] = array(

        '#type' => 'weight',
        '#default_value' => $items[$delta]['weight'],
        '#title' => t('Weight'),
        '#delta' => 50,
        '#access' => $items[$delta]['menu_name'] != MENU_LINK_EMPTY_KEY,
      );
      break;
  }

  return $element;
}

/* Formatters */

/**
 * Implements hook_field_formatter_info().
 */
function menu_link_field_formatter_info() {

  return array(

    'menu_link_formatter' => array(

      'label' => t('Default'),
      'field types' => array(MENU_LINK_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function menu_link_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  switch ($display['type']) {

    case 'menu_link_formatter':
      // DISPLAY NOTHING
      break;
  }

  return $element;
}
